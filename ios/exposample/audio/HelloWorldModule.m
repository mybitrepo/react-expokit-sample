// CalendarManager.m
#import "HelloWorldModule.h"
#import <React/RCTLog.h>

@implementation HelloWorldModule

// To export a module named HelloWorld. Add no arguments if you want to export with the name HelloWorldModule.
RCT_EXPORT_MODULE(HelloWorld);

RCT_EXPORT_METHOD(helloWorld:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
    resolve(@"Hello world from Objective-C side");
}

@end
