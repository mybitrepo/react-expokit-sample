import React from 'react';
import {
  Button,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {DangerZone, WebBrowser} from 'expo';
import HelloWorld from '../native/HelloWorld';
import SuperPoweredModule from '../native/SuperPoweredModule';

const { Lottie } = DangerZone;

export default class HomeScreen extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      helloText: 'Loading java hello...',
      helloTextNative: 'Loading C++ hello...',
      helloTextSuperPowered: 'Loading C++ SuperPowered hello...',
      init: null,
      path: null,
      clickTime: 0,
      latency: 0,
    };
  }
  
  render() {
    return (
      <View style={styles.container}>
        
        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <Button title="Play" onPress={this._playAudio}/>
          <Button color={"red"} title="Pause" onPress={this._pauseAudio}/>
        </View>
        
        <View style={{ alignSelf:'baseline', flexDirection:'row'}}>
          <Button title="Choose File" onPress={this._chooseFile}/>
          <Text style={{paddingLeft:10, paddingTop:10}}>File: {this.state.path}</Text>
        </View>
  
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          
          <Button title="Play Animation" onPress={this._playAnimation}/>
          
          <Lottie
            imageAssetsFolder='images'
            autoPlay={true}
            style={styles.lottie}
            ref={animation => {
              this.animation = animation;
            }}
            loop={true}
            source={require('../assets/lottie/blue_bg_active.json')}
          />
          
          <Text>Latency: {this.state.latency}</Text>
          <Text>{this.state.helloText}</Text>
          <Text>{this.state.helloTextNative}</Text>
          <Text>{this.state.helloTextSuperPowered}</Text>
        
        </ScrollView>
      </View>
    );
  }
  
  componentDidMount() {
    
    this.init();
    
    this._helloWorld().then((str) => {
      this.setState(() => {
        return {helloText: str};
      });
    });
    
    this._helloWorldNative().then((str) => {
      this.setState(() => {
        return {helloTextNative: str};
      })
    });
    
    this._helloWorldSuperPowered().then((str) => {
      this.setState(() => {
        return {helloTextSuperPowered: str};
      })
    });
    
  }
  
  async init() {
    try {
      let init = await SuperPoweredModule.init();
      this.setState({init});
    } catch (e) {
      console.error(e);
    }
  }
  
  async _helloWorld() {
    let helloStr = ' Empty... ';
    try {
      helloStr = await HelloWorld.helloWorld();
    } catch (e) {
      console.error(e);
    }
    return helloStr;
  }
  
  async _helloWorldNative() {
    let helloStr = ' Empty... ';
    try {
      //TODO: Resolve this in iOS side
      //helloStr = await HelloWorld.helloWorldNative();
    } catch (e) {
      console.error(e);
    }
    return helloStr;
  }
  
  async _helloWorldSuperPowered() {
    let helloStr = ' Empty... ';
    try {
      //TODO: resolve this in iOS side
      //helloStr = await SuperPoweredModule.helloWorld();
    } catch (e) {
      console.error(e);
    }
    return helloStr;
  }
  
  _playAnimation = () => {
    this.animation.play();
  };
  
  _playAudio = () => {
    
    this.setState({
      clickTime: (new Date()).getTime()
    });
    
    try {
      SuperPoweredModule.startPlayer().then(result => {
        console.log(result);
        this.setState({latency: (result - this.state.clickTime)});
      })
    } catch (e) {
      console.error(e);
    }
  };
  
  _pauseAudio = () => {
  
    this.setState({
      clickTime: (new Date()).getTime()
    });
  
    try {
      SuperPoweredModule.pausePlayer().then(result => {
        console.log(result);
        this.setState({latency: (result - this.state.clickTime)});
      })
    } catch (e) {
      console.error(e);
    }
  };
  
  _chooseFile() {
    Alert.alert("No file picker available!");
  }
  
  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );
      
      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }
  
  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };
  
  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fffaaa',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  lottie: {
    marginTop: 30,
    alignSelf: 'center',
    width: 200,
    height: 200,
  },
});
