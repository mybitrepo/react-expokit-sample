package host.exp.exponent.audio;

import android.content.Context;
import android.media.AudioManager;
import android.os.Build;
import android.os.Environment;

import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.io.File;
import java.io.IOException;

public class SuperPoweredModule extends ReactContextBaseJavaModule implements LifecycleEventListener {

    private static final String TAG = "mytag";

    static {
        System.loadLibrary("native-audio-lib");
    }

    public SuperPoweredModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "SuperPoweredModule";
    }

    @Override
    public void onHostResume() {

    }

    @Override
    public void onHostPause() {

    }

    @Override
    public void onHostDestroy() {

    }

    @ReactMethod
    public void init(Promise promise) {
        // Get the device's sample rate and buffer size to enable low-latency Android audio output, if available.
        String sampleRateString = null;
        String bufferSizeString = null;
        if (Build.VERSION.SDK_INT >= 17) {
            AudioManager audioManager = (AudioManager) getReactApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            sampleRateString = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE);
            bufferSizeString = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_FRAMES_PER_BUFFER);
        }
        if (sampleRateString == null) sampleRateString = "44100";
        if (bufferSizeString == null) bufferSizeString = "512";
        int sampleRate = Integer.parseInt(sampleRateString);
        int bufferSize = Integer.parseInt(bufferSizeString);

        NativeAudioEngine(sampleRate, bufferSize);

        String path;
        try {
            path = Environment.getExternalStorageDirectory().getCanonicalPath()
                    + File.separator + "Music" + File.separator + "lycka.mp3";
            createPlayer(0, path, 128, 0);
            createPlayer(1, path, 128, 0);
            createPlayer(2, path, 128, 0);
            createPlayer(3, path, 128, 0);
            createPlayer(4, path, 128, 0);
            createPlayer(5, path, 128, 0);
            createPlayer(6, path, 128, 0);
            createPlayer(7, path, 128, 0);
            createPlayer(8, path, 128, 0);
            createPlayer(9, path, 128, 0);
            createPlayer(10, path, 128, 0);
            createPlayer(11, path, 128, 0);
            createPlayer(12, path, 128, 0);
            createPlayer(13, path, 128, 0);
            createPlayer(14, path, 128, 0);
            createPlayer(15, path, 128, 0);
        } catch (IOException e) {
            e.printStackTrace();
        }


        promise.resolve("true");
    }

    @ReactMethod
    public void startPlayer(Promise promise) {
        startPlayer(0);
        startPlayer(1);
        startPlayer(2);
        startPlayer(3);
        startPlayer(4);
        startPlayer(5);
        startPlayer(6);
        startPlayer(7);
        startPlayer(8);
        startPlayer(9);
        startPlayer(10);
        startPlayer(11);
        startPlayer(12);
        startPlayer(13);
        startPlayer(14);
        startPlayer(15);
        promise.resolve(String.valueOf(System.currentTimeMillis()));
    }

    @ReactMethod
    public void pausePlayer(Promise promise) {
        pausePlayer(0);
        pausePlayer(1);
        pausePlayer(2);
        pausePlayer(3);
        pausePlayer(4);
        pausePlayer(5);
        pausePlayer(6);
        pausePlayer(7);
        pausePlayer(8);
        pausePlayer(9);
        pausePlayer(10);
        pausePlayer(11);
        pausePlayer(12);
        pausePlayer(13);
        pausePlayer(14);
        pausePlayer(15);
        promise.resolve(String.valueOf(System.currentTimeMillis()));
    }

    @ReactMethod
    public void helloWorld(Promise promise) {
        try {
            String hello = helloWorld();
            promise.resolve(hello);
        } catch (Exception e) {
            promise.reject("ERR", e);
        }
    }

    private native void NativeAudioEngine(int samplerate, int buffersize);

    private native void destroyEngine();

    private native void onPlayPause(int playerId, boolean play);

    private native void startPlayer(int playerToStart);

    private native void startSyncingPlayer(int playerToStart);

    private native void preparePlayer(int playerId, double timeToStartInMs);

    private native void prepareSyncingPlayer(double timeToStartInMs);

    private native void preparePlayerWithPitch(int playerId, double timeToStartInMs, int pitchShift);

    private native void createPlayer(int playerId, String fileToPlay, float bpm, int pitchShift);

    private native void createSyncingPlayer(int playerId, String fileToPlay, float bpm, int pitchShift);

    private native void pausePlayer(int playerId);

    private native void pauseSyncingPlayer();

    private native void detectBPM(); //currently unused

    private native double getCurrentTimeForPlayer(int playerId);

    private native boolean isPlayerPlaying(int playerId);

    private native int getTrackDuration(int playerId);

    private native void setPlayerVolume(int playerId, float volume);

    private native void setPlayerPitch(int playerId, int pitchShiftInSemiTones);

    public native String helloWorld();
}
