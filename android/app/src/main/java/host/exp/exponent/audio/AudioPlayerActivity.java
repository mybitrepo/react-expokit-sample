package host.exp.exponent.audio;

import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public class AudioPlayerActivity extends AppCompatActivity {

    static {
        System.loadLibrary("native-audio-lib");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String samplerateString = null, buffersizeString = null;
        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            samplerateString = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE);
            buffersizeString = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_FRAMES_PER_BUFFER);
        }
        if (samplerateString == null) samplerateString = "48000";
        if (buffersizeString == null) buffersizeString = "512";
        int sampleRate = Integer.parseInt(samplerateString);
        int bufferSize = Integer.parseInt(buffersizeString);
        NativeAudioEngine(sampleRate, bufferSize);
    }


    // Functions implemented in the native library.
    private native void NativeAudioEngine(int samplerate, int buffersize);
    private native void destroyEngine();
    private native void onPlayPause(int playerId, boolean play);
    private native void startPlayer(int playerToStart);
    private native void startSyncingPlayer(int playerToStart);
    private native void preparePlayer(int playerId, double timeToStartInMs);
    private native void prepareSyncingPlayer(double timeToStartInMs);
    private native void preparePlayerWithPitch(int playerId, double timeToStartInMs, int pitchShift);
    private native void createPlayer(int playerId, String fileToPlay, float bpm, int pitchShift);
    private native void createSyncingPlayer(int playerId, String fileToPlay, float bpm, int pitchShift);
    private native void pausePlayer(int playerId);
    private native void pauseSyncingPlayer();
    private native void detectBPM(); //currently unused
    private native double getCurrentTimeForPlayer (int playerId);
    private native boolean isPlayerPlaying (int playerId);
    private native int getTrackDuration(int playerId);
    private native void setPlayerVolume(int playerId, float volume);
    private native void setPlayerPitch(int playerId, int pitchShiftInSemiTones);
}
