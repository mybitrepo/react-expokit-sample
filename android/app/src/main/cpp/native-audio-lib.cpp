#include "native-audio-lib.h"
#include <SuperpoweredSimple.h>
#include <SuperpoweredCPU.h>
#include <jni.h>
#include <stdio.h>
#include <android/log.h>
#include <stdlib.h>
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_AndroidConfiguration.h>


#define LOG_TAG "my_log_tag"
//#include <cutils/log.h>

#define log_print __android_log_print

// This is called by player A upon successful load.
static void playerEventCallbackA (
        void *clientData,   // &player
        SuperpoweredAdvancedAudioPlayerEvent event,
        void * __unused value
) {
    SuperpoweredAdvancedAudioPlayer *player = *((SuperpoweredAdvancedAudioPlayer **)clientData);

    if (event == SuperpoweredAdvancedAudioPlayerEvent_LoadSuccess) {
        // The pointer to the player is passed to the event callback via the custom clientData pointer.
        // level 2 bpm = 152, sample offset = 40
        // level 1 bpm = 126, sample offset = 21
        //player->setBpm(cBPM);
        player->setFirstBeatMs(0);
        player->setPosition(player->firstBeatMs, true, true);
        __android_log_print(ANDROID_LOG_INFO, "bpm-log", "%f", player->currentBpm);

    } else if (event == SuperpoweredAdvancedAudioPlayerEvent_EOF) {
        player->pause();
        player->setPosition(player->firstBeatMs, true, true);
        __android_log_print(ANDROID_LOG_INFO, "log", "reached end of file");

    }
    //else {
    //  throw an exception if load failed
    //}
}

// Audio callback function. Called by the audio engine.
static bool audioProcessing (
        void *clientdata,		    // A custom pointer your callback receives.
        short int *audioIO,		    // 16-bit stereo interleaved audio input and/or output.
        int numFrames,			    // The number of frames received and/or requested.
        int __unused samplerate	    // The current sample rate in Hz.
) {
//*self =
//
//    bool silence = !this->playerA->process(this->stereoBuffer, false, numberOfSamples, self->volA, masterBpm, self->playerB->msElapsedSinceLastBeat);
//    if (self->playerB->process(self->stereoBuffer, !silence, numberOfSamples, self->volB, masterBpm, msElapsedSinceLastBeatA)) silence = false;
//
//    if (!silence) SuperpoweredDeInterleave(self->stereoBuffer, buffers[0], buffers[1], numberOfSamples); // The stereoBuffer is ready now, let's put the finished audio into the requested buffers.
//    return !silence;

    return ((NativeAudioEngine *)clientdata)->process(audioIO, (unsigned int)numFrames);
}

// Main process function where audio is generated
bool NativeAudioEngine::process (
        short int *output,         // buffer to receive output samples
        unsigned int numFrames     // number of frames requested
) {
    double masterBpm = players[0]->currentBpm;
    double msElapsedSinceLastBeat = syncingPlayer->msElapsedSinceLastBeat;

    // Request audio from player A.
    bool silence = !players[0]->process (
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            false,         // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[0],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat // ms elapsed since the last beat on the other track.
    );

    // Request audio from player B.
    if (players[1]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[1],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    if (players[2]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[2],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    if (players[3]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[3],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    if (players[4]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[4],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    if (players[5]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[5],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    if (players[6]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[6],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    if (players[7]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[7],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    if (players[8]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[8],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    if (players[9]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[9],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    if (players[10]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[10],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    if (players[11]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[11],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    if (players[12]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[12],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    if (players[13]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[13],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    if (players[14]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[14],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    if (players[15]->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            playerVolume[15],          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    )) silence = false;

    syncingPlayer->process(
            stereoBuffer,  // 32-bit interleaved stereo output buffer.
            !silence,      // bufferAdd - true: add to buffer / false: overwrite buffer
            numFrames,     // The number of frames to provide.
            0.0f,          // volume - 0.0f is silence, 1.0f is "original volume"
            masterBpm,     // BPM value to sync with.
            players[0]->msElapsedSinceLastBeat   // ms elapsed since the last beat on the other track.
    );


    // The stereoBuffer is ready now, let's write the finished audio into the requested buffers.
    if (!silence) SuperpoweredFloatToShortInt(stereoBuffer, output, numFrames);
    return !silence;
}

// Crossfader example - Initialize player and audio engine
NativeAudioEngine::NativeAudioEngine (
        unsigned int samplerate,    // sampling rate
        unsigned int buffersize    // buffer size
) {
    // Allocate aligned memory for floating point buffer.
    stereoBuffer = (float *) memalign(16, buffersize * sizeof(float) * 2);
    //sampleRate = samplerate;

    //assign buffersize and samplerate variables from java to variables within c++ scope
    sampleRate = samplerate;
    bufferSize = buffersize;

    for (unsigned int i = 0; i <= 15; ++i) {
        players[i] = new SuperpoweredAdvancedAudioPlayer(&players[i], playerEventCallbackA, sampleRate, 0);
        playerVolume[i] = 0.8f;
    }

    syncingPlayer = new SuperpoweredAdvancedAudioPlayer(&syncingPlayer, playerEventCallbackA, sampleRate, 0);

    // Initialize audio engine and pass callback function.
    audioSystem = new SuperpoweredAndroidAudioIO (
            sampleRate,                     // sampling rate
            bufferSize,                     // buffer size
            false,                          // enableInput
            true,                           // enableOutput
            audioProcessing,                // audio callback function
            this,                           // clientData
            -1,                             // inputStreamType (-1 = default)
            SL_ANDROID_STREAM_MEDIA,        // outputStreamType (-1 = default)
            buffersize * 2                  // latency (frames)
    );
}

// Destructor. Free resources.
NativeAudioEngine::~NativeAudioEngine() {
    __android_log_print(ANDROID_LOG_INFO, "c-log", "deleting engine");

    delete audioSystem;

    for (unsigned int i = 0; i <= 15; ++i) {
        delete players[i];
    }

    delete syncingPlayer;

    free(stereoBuffer);
}

// onPlayPause - Toggle playback state of players.
void NativeAudioEngine::onPlayPause(int playerId, bool play) {
    if (!play) {
        players[playerId]->pause();
    } else {
        players[playerId]->play(true);
    }
    SuperpoweredCPU::setSustainedPerformanceMode(play); // <-- Important to prevent audio dropouts.
}

void NativeAudioEngine::startPlayer(int playerToStart)
    {
        __android_log_print(ANDROID_LOG_INFO, "c-log", "about to start player");
        players[playerToStart]->play(true);
        __android_log_print(ANDROID_LOG_INFO, "c-log", "starting player");

        SuperpoweredCPU::setSustainedPerformanceMode(true); // <-- Important to prevent audio dropouts.

        if (players[playerToStart]->playing) {
            __android_log_print(ANDROID_LOG_INFO, "log", "player started");
        } else {
            __android_log_print(ANDROID_LOG_INFO, "log", "player not started");
        }

        float startTime = players[playerToStart]->positionMs;

        __android_log_print(ANDROID_LOG_INFO, "c-log", "player %i started at: %f", playerToStart, startTime);

    }

void NativeAudioEngine::startSyncingPlayer(int playerToStart)
{
//    syncingPlayer->setPosition(
//            34.f,     // seek position in ms
//            true,               // stop playback? true = stops playback
//            true,                // synchronised start
//            false,               // force default quantum
//            true                 // prefer waiting for sync start
//    );
    syncingPlayer->play(true);
    //SuperpoweredCPU::setSustainedPerformanceMode(true); // <-- Important to prevent audio dropouts.

    __android_log_print(ANDROID_LOG_INFO, "c-log", "syncing player started");
}

void NativeAudioEngine::preparePlayer(int playerId, double timeToStartInMs)
{
    players[playerId]->setPosition(
            timeToStartInMs,     // seek position in ms
            true,               // stop playback? true = stops playback
            true,                // synchronised start
            false,               // force default quantum
            true                 // prefer waiting for sync start
    );
    __android_log_print(ANDROID_LOG_INFO, "c-log", "player %i prepared with time: %f", playerId, timeToStartInMs);
}

void NativeAudioEngine::prepareSyncingPlayer(double timeToStartInMs)
{
    syncingPlayer->setPosition(
            timeToStartInMs,     // seek position in ms
            true,               // stop playback? true = stops playback
            true,                // synchronised start
            false,               // force default quantum
            true                 // prefer waiting for sync start
    );
    //__android_log_print(ANDROID_LOG_INFO, "c-log", "player %i prepared with time: %f", playerId, timeToStartInMs);
}

void NativeAudioEngine::preparePlayerWithPitch(int playerId, double timeToStartInMs, int pitchShift)
{
    if (pitchShift != 0) {
        players[playerId]->setPitchShift(pitchShift);
    }

    players[playerId]->setPosition(
            timeToStartInMs,     // seek position in ms
            true,               // stop playback? true = stops playback
            true,                // synchronised start
            false,               // force default quantum
            true                 // prefer waiting for sync start
    );
}

double NativeAudioEngine::getCurrentTimeForPlayer(int playerId)
{
    return players[playerId]->positionMs;
}

#define MINFREQ 60.0f
#define MAXFREQ 20000.0f

static inline float floatToFrequency(float value) {
    if (value > 0.97f) return MAXFREQ;
    if (value < 0.03f) return MINFREQ;
    value = powf(10.0f, (value + ((0.4f - fabsf(value - 0.4f)) * 0.3f)) * log10f(MAXFREQ - MINFREQ)) + MINFREQ;
    return value < MAXFREQ ? value : MAXFREQ;
}

static NativeAudioEngine *example = NULL;

void NativeAudioEngine::createPlayer(int playerId, const char *path, float bpm, int pitchShift)
{

    players[playerId]->open(path);
    players[playerId]->setSamplerate(sampleRate);
    players[playerId]->setBpm(bpm);
    players[playerId]->syncMode = SuperpoweredAdvancedAudioPlayerSyncMode_TempoAndBeat;
    players[playerId]->setPitchShift(pitchShift);
    players[playerId]->fixDoubleOrHalfBPM = true;
    players[playerId]->setFirstBeatMs(4);

    if (playerId == 0 || playerId == 4 || playerId == 8 || playerId == 12) {
        __android_log_print(ANDROID_LOG_INFO, "c-log", "song: %d tempo = %f", playerId, players[playerId]->currentBpm);
    }

    __android_log_print(ANDROID_LOG_INFO, "log", "player id: %d \n", playerId);
    __android_log_print(ANDROID_LOG_INFO, "log", "bpm: %f \n", bpm);

}

void NativeAudioEngine::createSyncingPlayer(int playerId, const char *path, float bpm, int pitchShift)
{
    syncingPlayer->open(path);
    syncingPlayer->setSamplerate(sampleRate);
    syncingPlayer->setBpm(bpm);
    syncingPlayer->syncMode = SuperpoweredAdvancedAudioPlayerSyncMode_TempoAndBeat;
    syncingPlayer->setFirstBeatMs(4);
}

void NativeAudioEngine::pausePlayer(int playerId)
{
    players[playerId]->pause();
}

void NativeAudioEngine::pauseSyncingPlayer()
{
    syncingPlayer->pause();
}

void NativeAudioEngine::detectBPM()
{
//    analyzer = new SuperpoweredAnalyzer(
//            unsigned int 	sampleRate,
//            float 	bpm = 0,
//            int 	lengthSeconds = 0,
//            float 	minimumBpm = 60.0f,
//            float 	minimumBpm = 60.0f,
//            float 	maximumBpm = 200.0f
//    )
//
//    analyzer->process();
//
//    analyzer->getresults(bpm);
//
//
}

bool NativeAudioEngine::isPlayerPlaying(int playerId)
{
    bool isPlaying = false;
    players[playerId]->playing ? isPlaying : !isPlaying;
    return isPlaying;
}

int NativeAudioEngine::getTrackDuration(int playerId)
{
    return players[playerId]->durationMs;
}

void NativeAudioEngine::setPlayerLoop(int playerId, double startMs, double endMs)
{
    players[playerId]->loopBetween(
            startMs,      // loop start point
            endMs,        // loop end point
            true,         // jump to start point if playhead has passed it when called
            255,          // point ID for referencing loop segment again later?
            true,         // synchronised start
            false,        // forcedefaultquantum - superpowered future use
            false          // prefer waiting for synchronised start
    );
    //players[playerId]->exitLoop(true);
}

void NativeAudioEngine::setPlayerVolume(int playerId, float volume) 
{
    playerVolume[playerId] = volume;
}

void NativeAudioEngine::setPlayerPitch(int playerId, int pitchShiftInSemiTones)
{
    players[playerId]->setPitchShift(pitchShiftInSemiTones);
}



// NativeAudioEngine- Create the DJ app and initialize the players.
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_NativeAudioEngine (
        JNIEnv *env,
        jobject __unused obj,
        jint samplerate,        // sampling rate
        jint buffersize        // buffer size
) {
    example = new NativeAudioEngine((unsigned int)samplerate, (unsigned int)buffersize);
}

// destroyEngine- Calls destructor for native audio engine.
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_destroyEngine (
        JNIEnv *env,
        jobject __unused obj
) {
    example->~NativeAudioEngine();
}


// onPlayPause - Toggle playback state of player.
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_onPlayPause (
        JNIEnv * __unused env,
        jobject __unused obj,
        jint playerId,
        jboolean play
) {
    example->onPlayPause(playerId, play);
}

// startPlayer - select player and begin playing!
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_startPlayer (
        JNIEnv * __unused env,
        jobject __unused obj,
        jint playerToStart
) {
    example->startPlayer(playerToStart);
}

// startSyncingPlayer - select player and begin playing!
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_startSyncingPlayer (
        JNIEnv * __unused env,
        jobject __unused obj,
        jint playerToStart
) {
    example->startSyncingPlayer(playerToStart);
}

// preparePlayer - Select a player and give it a start time in ms
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_preparePlayer(
        JNIEnv * __unused env,
        jobject __unused obj,
        jint playerId,
        jdouble timeToStartInMs
) {
    example->preparePlayer(playerId, timeToStartInMs);
}

// prepareSyncingPlayer - Select a player and give it a start time in ms
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_prepareSyncingPlayer(
        JNIEnv * __unused env,
        jobject __unused obj,
        jdouble timeToStartInMs
) {
    example->prepareSyncingPlayer(timeToStartInMs);
}

// preparePlayer - Select a player and give it a start time in ms
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_preparePlayerWithPitch(
        JNIEnv * __unused env,
        jobject __unused obj,
        jint playerId,
        jdouble timeToStartInMs,
        jint pitchShift
) {
    example->preparePlayerWithPitch(playerId, timeToStartInMs, pitchShift);
}

// createPlayer - add new player superpowered audio player
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_createPlayer (
        JNIEnv * __unused env,
        jobject __unused obj,
        jint playerId,
        jstring fileToPlay,     // path to file to play
        jfloat bpm,
        jint pitchShift
) {
    const char *path = env->GetStringUTFChars(fileToPlay, JNI_FALSE);
    env->ReleaseStringUTFChars(fileToPlay, path);
    example->createPlayer(playerId, path, bpm, pitchShift);
}

// createSyncingPlayer - creates an extra player used only for syncing main players
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_createSyncingPlayer (
        JNIEnv * __unused env,
        jobject __unused obj,
        jint playerId,
        jstring fileToPlay,     // path to file to play
        jfloat bpm,
        jint pitchShift
) {
    const char *path = env->GetStringUTFChars(fileToPlay, JNI_FALSE);
    env->ReleaseStringUTFChars(fileToPlay, path);
    example->createSyncingPlayer(playerId, path, bpm, pitchShift);
}

// pausePlayer - free player to be used again
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_pausePlayer (
        JNIEnv * __unused env,
        jobject __unused obj,
        jint playerId
) {
    example->pausePlayer(playerId);
}

// pauseSyncingPlayer - pauses the syncing player
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_pauseSyncingPlayer (
        JNIEnv * __unused env,
        jobject __unused obj
) {
    example->pauseSyncingPlayer();
}

// detectBPM - takes a reference to track and detects bpm.
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_detectBPM (
        JNIEnv * __unused env,
        jobject __unused obj
) {
    example->detectBPM();
}

extern "C" JNIEXPORT jdouble
Java_host_exp_exponent_audio_SuperPoweredModule_getCurrentTimeForPlayer(
        JNIEnv * __unused env,
        jobject __unused obj,
        jint playerId
) {
    return example->getCurrentTimeForPlayer(playerId);
}

// isPlayerPlaying - returns true if specified player is currently playing.
extern "C" JNIEXPORT jboolean
Java_host_exp_exponent_audio_SuperPoweredModule_isPlayerPlaying(
        JNIEnv * __unused env,
        jobject __unused obj,
        jint playerId
) {
    return example->isPlayerPlaying(playerId);
}

// getTrackDuration - returns duration of song loaded into player in milliseconds
extern "C" JNIEXPORT jint
Java_host_exp_exponent_audio_SuperPoweredModule_getTrackDuration(
        JNIEnv * __unused env,
        jobject __unused obj,
        jint playerId
) {
    return example->getTrackDuration(playerId);
}

// setPlayerLoop - set loop start and end points for a given player
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_setPlayerLoop (
        JNIEnv * __unused env,
        jobject __unused obj,
        jint playerId,
        jdouble startMs,
        jdouble endMs

) {
    example->setPlayerLoop(playerId, startMs, endMs);
  }

// setPlayerLoop - set loop start and end points for a given player
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_setPlayerVolume (
        JNIEnv * __unused env,
        jobject __unused obj,
        jint playerId,
        jfloat volume

) {
    example->setPlayerVolume(playerId, volume);
}

// setPlayerPitch - pitch of player in semitones
extern "C" JNIEXPORT void
Java_host_exp_exponent_audio_SuperPoweredModule_setPlayerPitch (
        JNIEnv * __unused env,
        jobject __unused obj,
        jint playerId,
        jint pitchShift
) {
    example->setPlayerPitch(playerId, pitchShift);
}

extern "C" JNIEXPORT jstring
Java_host_exp_exponent_audio_SuperPoweredModule_helloWorld(
        JNIEnv *__unused env,
        jobject __unused obj
) {
    return env->NewStringUTF("Hello World! I am coming from SuperPoweredModule C++");
}