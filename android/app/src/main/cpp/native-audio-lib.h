//
// Created by Joseph Horton on 05/06/2018.
//

#ifndef NUSIC_ANDROID_NATIVE_AUDIO_LIB_H
#define NUSIC_ANDROID_NATIVE_AUDIO_LIB_H

#include <math.h>
#include <pthread.h>
#include <iostream>
#include <vector>
#include <SuperpoweredAdvancedAudioPlayer.h>
#include <SuperpoweredAnalyzer.h>
#include <SuperpoweredSimple.h>
#include "SuperpoweredAndroidAudioIO.h"
#include <stdio.h>
#include <jni.h>

struct player {
    int id;
    float volume;
    bool isPlaying;
    SuperpoweredAdvancedAudioPlayer *playerRef;
};

#define HEADROOM_DECIBEL 3.0f
static const float headroom = powf(10.0f, -HEADROOM_DECIBEL * 0.025f);

class NativeAudioEngine {
public:

    NativeAudioEngine(unsigned int samplerate, unsigned int buffersize);
    ~NativeAudioEngine();

    bool process(short int *output, unsigned int numberOfSamples);
    void onPlayPause(int playerId, bool play);
    void startPlayer(int playerToStart);
    void startSyncingPlayer(int playerToStart);
    void preparePlayer(int playerId, double timeToStartInMs);
    void prepareSyncingPlayer(double timeToStartInMs);
    void preparePlayerWithPitch(int PlayerId, double timeToStartInMs, int pitchShift);
    void pausePlayer(int playerId);
    void pauseSyncingPlayer();
    void startPlayerAtTime(int playerToStart, int timeToStart);
    void createPlayer(int playerId, const char *path, float bpm, signed int pitchShift);
    void createSyncingPlayer(int playerId, const char *path, float bpm, signed int pitchShift);
    //void releasePlayer(int playerId);
    void stopPlayer(int playerToStop);
    void detectBPM();
    double getCurrentTimeForPlayer(int playerId);
    bool isPlayerPlaying (int playerId);
    int getTrackDuration (int playerId);
    void setPlayerLoop(int playerId, double startMs, double endMs);
    void setPlayerVolume(int playerId, float volume);
    void setPlayerPitch(int playerId, int pitchShiftInSemiTones);
    //unsigned int sampleRate;


private:
    SuperpoweredAndroidAudioIO *audioSystem;
    //SuperpoweredAdvancedAudioPlayer *player;
    //std::vector<SuperpoweredAdvancedAudioPlayer*> players;
    SuperpoweredAdvancedAudioPlayer *players[16]; //set up 16 audio players
    SuperpoweredAdvancedAudioPlayer *syncingPlayer;
    SuperpoweredAnalyzer *analyzer;
    float playerVolume[16];

    player playerData[16];
    float *stereoBuffer;

    unsigned int sampleRate;
    unsigned int bufferSize;

};

#endif //NUSIC_ANDROID_NATIVE_AUDIO_LIB_H