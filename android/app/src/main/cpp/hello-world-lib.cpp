#include <jni.h>

extern "C" JNIEXPORT jstring
Java_host_exp_exponent_audio_HelloWorldModule_helloWorldJNI(
        JNIEnv *__unused env,
        jobject __unused obj
) {
    return env->NewStringUTF("Hello World! I am coming from C++ side");
}